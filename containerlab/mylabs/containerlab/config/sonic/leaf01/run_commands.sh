#!/bin/bash
mkdir /etc/apt/keyrings
curl -fsSL -o /etc/apt/keyrings/salt-archive-keyring-2023.gpg https://repo.saltproject.io/salt/py3/debian/11/amd64/SALT-PROJECT-GPG-PUBKEY-2023.gpg
echo "deb [signed-by=/etc/apt/keyrings/salt-archive-keyring-2023.gpg arch=amd64] https://repo.saltproject.io/salt/py3/debian/11/amd64/latest bullseye main" | tee /etc/apt/sources.list.d/salt.list
apt-get update && apt-get install -y salt-minion
cat <<EOF > /etc/salt/minion
master: 172.17.0.1
EOF
/usr/bin/salt-minion -d

config vlan add 10
config vlan member add -u 10 Ethernet8
config interface ip add Loopback0 10.255.255.1/32
config interface startup Loopback0
config interface ip add Ethernet0 10.0.30.2/30
config interface startup Ethernet0
config interface ip add Ethernet4 10.0.40.2/30
config interface startup Ethernet4
config interface startup Ethernet8
config save -y
# vtysh -c "
# conf t
# router bgp 65000
#  bgp router-id 10.255.255.1
#  bgp log-neighbor-changes
#  no bgp ebgp-requires-policy
#  no bgp default ipv4-unicast
#  no bgp network import-check
#  neighbor evpn peer-group
#  neighbor evpn remote-as 65000
#  neighbor 10.0.30.1 peer-group evpn
#  neighbor 10.0.40.1 peer-group evpn
#  !
#  address-family ipv4 unicast
#   redistribute connected
#   neighbor evpn activate
#   neighbor evpn next-hop-self
#   maximum-paths ibgp 4
#  exit-address-family
#  !
#  address-family l2vpn evpn
#   neighbor evpn activate
#   neighbor evpn next-hop-self
#   advertise-all-vni
#  exit-address-family
# exit
# !
# end
# exit"
